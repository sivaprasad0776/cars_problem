function problem1(inventory, id) {
    if (! (inventory instanceof Array) || (typeof id != 'number')){
        return [];
    }

    const index = getIndex(id, inventory);
    if (index == -1) {
        return [];
    }
    else {
        return inventory[index];
    }
}

module.exports = problem1


function getIndex(id, inventory) {
    let pos = -1;
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id == id) {
            pos = i;
            break;
        }
    }
    return pos;
}
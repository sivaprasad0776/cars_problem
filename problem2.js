function problem2(inventory) {
    if (! (inventory instanceof Array)){
        return [];
    }

    const index = getLastIndex(inventory);
    if (index == -1) {
        return [];
    }
    else {
        return inventory[index];

    }
}

module.exports = problem2;


function getLastIndex(inventory) {
    let pos = -1
    let max = -1;
    for (let i = 0; i < inventory.length; i++) {
        if (max < inventory[i].id) {
            max = inventory[i].id;
            pos = i;
        }
    }
    return pos;
}
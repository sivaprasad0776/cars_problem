function problem3(inventory){
    if (! (inventory instanceof Array) || inventory.length == 0){
        return [];
    }
    let models = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let model = inventory[i].car_model.trim().toUpperCase();
        models.push(model)
    }
    
    const sortedModels = models.sort();
    const modelsSet = new Set(sortedModels);
    const finalModels = [...modelsSet]
    
    return finalModels;
}

module.exports = problem3;


function problem4(inventory){
    if (! (inventory instanceof Array) || inventory.length == 0){
        return [];
    }
    let years = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let year = inventory[i].car_year ;
        years.push(year);
    }
    
    return years.sort();
}

module.exports = problem4;


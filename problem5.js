const pr4 = require('./problem4');
function problem5(inventory){
    if (! (inventory instanceof Array) || inventory.length == 0){
        return [];
    }
    const years = pr4(inventory);
    
    let carsBefore2000 = new Array();
    for ( let i = 0; i < years.length ; i++ ){
        if(years[i] < 2000){
            carsBefore2000.push(years[i]);
        }
        else{
            break;
        }
    }
    
    return carsBefore2000;
    
}

module.exports = problem5;
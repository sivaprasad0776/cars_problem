function problem6(inventory){
    if (! (inventory instanceof Array) || inventory.length == 0){
        return [];
    }
    let bmwAduiCars = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let make = inventory[i].car_make .trim().toUpperCase();
        if(make == 'BMW' || make == 'AUDI'){
            bmwAduiCars.push(inventory[i]);
        }
    }
    
    return bmwAduiCars;
    
}

module.exports = problem6;

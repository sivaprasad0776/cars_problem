const inventory = require('../cars_problem');
const problem6 = require('../problem6');
const result = problem6(inventory);

if(result.length == 0){
    console.log([]);
}
else{
    console.log(JSON.stringify([...result]));
}